package aed;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class MatrizFinitaTests
{   
    @Test
    public void testConstructor()
    {
        MatrizFinita matriz = new MatrizFinita(3,3);
        assertEquals(3, matriz.cantidadColumnas());
        assertEquals(3, matriz.cantidadFilas());
    }
    
    @Test
    public void testDefinirYObtener()
    {
        MatrizFinita A = new MatrizFinita(4,4);
        A.definir(2,2,43);
        A.definir(2,1,32);
        A.definir(2,3,51);
        A.definir(3,2,85);
        A.definir(2,2,43);

        assertEquals(43, A.obtener(2,2));
        assertEquals(32, A.obtener(2,1));
        assertEquals(51, A.obtener(2,3));
        assertEquals(85, A.obtener(3,2));
        assertEquals(43, A.obtener(2,2));
    }

    @Test
    public void testSumarMatrices1()
    {
        MatrizFinita A = new MatrizFinita(1,1);
        A.definir(0,0,1);

        MatrizFinita B = new MatrizFinita(1,1);
        B.definir(0,0,1);

        MatrizFinita matrizSuma = MatrizFinita.sumarMatrices(A,B);

        assertEquals(1, A.obtener(0,0));
        assertEquals(1, B.obtener(0,0));

        assertEquals(2, matrizSuma.obtener(0,0));
    }

    @Test
    public void testSumarMatrices2()
    {
        MatrizFinita A = new MatrizFinita(3,3);
        A.definir(0,1,1);
        A.definir(1,1,2);
        A.definir(2,1,1);

        MatrizFinita B = new MatrizFinita(3,3);
        B.definir(1,0,1);
        B.definir(1,1,3);

        MatrizFinita matrizSuma = MatrizFinita.sumarMatrices(A,B);

        assertEquals(1, A.obtener(0,1));
        assertEquals(2, A.obtener(1,1));
        assertEquals(1, B.obtener(1,0));
        assertEquals(3, B.obtener(1,1));

        assertEquals(1, matrizSuma.obtener(0,1));
        assertEquals(1, matrizSuma.obtener(1,0));
        assertEquals(5, matrizSuma.obtener(1,1));   
    }
    
    @Test
    public void testSumaMatrices3()
    {
        MatrizFinita A = new MatrizFinita(2,3);
        A.definir(1,0,1);
        A.definir(1,1,1);

        MatrizFinita B = new MatrizFinita(2,3);
        B.definir(0,1,1);
        B.definir(1,2,1);

        MatrizFinita matrizSuma = MatrizFinita.sumarMatrices(A,B);

        assertEquals(1, A.obtener(1,0));
        assertEquals(1, A.obtener(1,1));
        assertEquals(1, B.obtener(0,1));
        assertEquals(1, B.obtener(1,2));

        assertEquals(1, matrizSuma.obtener(0,1));
        assertEquals(1, matrizSuma.obtener(1,0));
        assertEquals(1, matrizSuma.obtener(1,1));
        assertEquals(1, matrizSuma.obtener(1,2));
    }
}
